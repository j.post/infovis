#Imports
import pandas as pd
import pycountry
import calendar

#Turning off errors for chained assignment.
pd.options.mode.chained_assignment = None

#Reading the csv file containing the airplane crashes.
csvData = pd.read_csv(r'infovis\data\Airplane_Crashes_and_Fatalities_Since_1908.csv')

#Removing the columns labelled "Flight #", "Time", "Route" , "cn/In" and "Registration".
#This is because these columns contain a lot of Not A Number values and are also not meaningful for our visualizations.
cleanedData = csvData.drop(['Flight #','Time', 'Route', 'cn/In', 'Registration'], axis=1)

#Dropping all rows that still contain Not A Number values.
dropNA = cleanedData.dropna()

#Splitting the Date column into three seperate columns so that sorting by year is easier.
dropNA['Date'] = pd.to_datetime(dropNA['Date'])
dropNA['Month'] = dropNA['Date'].dt.month
dropNA['Day'] = dropNA['Date'].dt.day
dropNA['Year'] = dropNA['Date'].dt.year

#Changing month number to month name - 1:January etc.
dropNA['Month'] = dropNA['Month'].apply(lambda x: calendar.month_name[x])

#Removing the Date column as it has become obsolete.
dateRemoved = dropNA.drop(['Date'], axis=1)

#Removing all flights before the year 1945 since this is over almost 80 years ago.
byYear = dateRemoved[(dateRemoved['Year'] >= 1945) & (dateRemoved['Year'] < 2009)]

#Removing the city location from the data and instead changing it into a column with just the country.
#This is done so that filtering by country is easier and since we will not be using exact locations.
byYear['Country'] = byYear['Location'].apply(lambda x: x.split(",")[-1].strip() if ',' in x else x)

#If a row in the column "Country" contains an inproper country name or a provence of Canada, change them to proper country names.
byYear['Country'] = byYear['Country'].replace({
    r'\bCA\b': 'Canada',
    r'\bGA\b': 'Gabon',
    r'\bIN\b': 'India',
    r'\bBO\b':'Bolivia',
    r'\bBoliva\b':'Bolivia',
    r'\bRussian\b': 'Russia',
    r'\bUK\b': 'United Kingdom',
    r'\bNewfoundland\b': 'Canada',
    r'\bIndian\b': 'India',
    r'\bHati\b': 'Haiti',
    r'\bLabrador\b': 'Canada',
    r'\bOkinawa\b': 'Japan',
    r'\bMartinique': 'France',
    r'\bReunion\b': 'France',
    r'\bManmar\b': 'Turkey',
    r'\bSaskatchewan\b': 'Canada',
    r'\bAmerican Samoa\b': 'United States of America',
    r'\bBritish Columbia\b': 'Canada',
    r'\bBritish Virgin Islands\b':'United Kingdom',
    r'\bFrench Polynesia\b':'France',
    r'\bSarawak\b': 'Malaysia',
    r'\bMariana Islands\b': 'United States of America',
    r'\bMarshall Islands\b': 'United States of America',
    r'\bGuadeloupe\b': 'France',
    r'\bManitoba\b': 'Canada',
    r'\bNew Guinea\b': 'Australia',
    r'\bSurinam\b': 'Suriname',
    r'\bTasmania\b': 'Australia',
    r'\bThe Netherlands\b': 'Netherlands',
    r'\bSantiago de Cuba\b':'Cuba',
    r'\bWales\b':'United Kingdom',
    r'\bEngland\b':'United Kingdom',
    r'\bScotland\b':'United Kingdom',
    r'\bNorthern Ireland\b':'United Kingdom',
    r'\bZaire\b':'Angola',
    r'\bAfrica\b': 'South Africa',    
    r'\bNorthern Ireland\b':'United Kingdom',
    r'\bDemocratiRepubliCongo\b':'Democratic Republic of the Congo'},regex=True)

#Replace values in "Summary" column
byYear['Summary'] = byYear['Summary'].replace({
    r'\bDemocratiRepubliCongo\b':'Democratic Republic of the Congo'},regex=True)

#Fix royal air force different spellings in the database
byYear['Operator'] = byYear['Operator'].replace({
    r'\bMilitary -Royal Air Force\b':'Military - Royal Air Force'},regex=True)

#List of all US States
state_names = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa',
    'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Massachusett', 'Michigan','Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
    'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma','Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee',
    'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming', 'AK', 'NY', 'Wake Island', 'HI', 'Guam', 'Virgin Islands', 'Virginia.']

#Checking if the column 'Country' contains a value of the state_names list and if so replaces it with 'United States of America'.
byYear['Country'][byYear['Country'].isin(state_names)] = 'United States of America'

#Check if a country is valid using pycountry
def valid_country(country):
    try:
        pycountry.countries.lookup(country)
        return True
    except LookupError:
        return False
    
def get_country_code(country):
    try:
        return pycountry.countries.search_fuzzy(country)[0].alpha_3
    except (LookupError,IndexError):
        return None

#Check if the 'Country' column values are valid countries and if not removes the rows containing invalid countries.
#Implemented since countries have changed names, seperated etc.
byYear['Country_code'] = byYear['Country'].apply(get_country_code)
byYear = byYear.dropna(subset=['Country_code'])

#Create a column containing the survivorcount
byYear['Survived'] = byYear['Aboard'] - byYear['Fatalities']

#Dropping the location column since it has been replaced with a country column.
finalData = byYear.drop(['Location'],axis=1)

finalData.loc[finalData['Country'] == 'Niger', 'Country_code'] = 'NER'

#Saving the filtered CSV data file.
finalData.to_csv('infovis\data\cleanedData.csv', index= False)

#Create new csv with unique countries and their total occurance
newCSV = finalData.drop(['Operator', 'Type', 'Fatalities', 'Aboard', 'Ground','Summary', 'Month', 'Day', 'Year', 'Survived'], axis= 1)

#Count the amount of times a countrycode occurs, drops the duplicates in the end.
country_count = newCSV['Country_code'].value_counts().reset_index()
country_count.columns = ['Country_code', 'Occurences']
#Remove duplicate rows from the new dataframe
unique_country_occurrences = country_count.drop_duplicates()

#Groups the finaldata DF by country_code and calculates the sum of all fatality and survived rows
#After that it merges the DF with the survivors/fatalities with the countries
country_totals = finalData.groupby('Country_code')['Fatalities', 'Survived'].sum().reset_index()
merged_data = pd.merge(unique_country_occurrences, country_totals)
merged_data[['Fatalities', 'Survived']] = merged_data[['Fatalities', 'Survived']].fillna(0)

#Removes duplicate Country/Country_code rows
country_names = newCSV[['Country_code', 'Country']].drop_duplicates()
merged_data = pd.merge(merged_data, country_names)

merged_data.to_csv('infovis\data\countryOccurences.csv', index= False)