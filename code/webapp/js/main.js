//Empty array for countries
var countries = []
//Read the CSV data and change strings to integers
d3.csv('../../data/cleanedData.csv', row => {
  row.year = +row.Year
  row.fatalities = +row.Fatalities
  row.totalSurvivors = +row.Survived
  
  if (!countries.includes(row.Country)){
    countries.push(row.Country)}

  return row
}).then(csv => {
  data = csv

  //Functionality behind the "Show world Information" button
  function allCountries(selectedYear,selectedCountry){
    circleMap.selectAll('*').remove()
    var selectedCountry = 'All countries'
    var selectedYear = '--ALL YEARS--'
  
    //Hide the map and show the circle map
    d3.select('#map-chart').style('display', 'none')
    d3.select('#buttonall-div').style('display', 'none')
    document.getElementById('country-name').innerHTML = 'Selected country: ' + selectedCountry
    circleMap.style('display', 'block')
  
    //Call the function to update charts with default values
    updateCharts(selectedYear, selectedCountry)
  
    //Filter data for all countries (remove the filter for a specific country)
    var allCrashes = data
  
    //Extract years from the filtered data for all countries
    var uniqueYears = [...new Set(allCrashes.map(entry => entry.year))]
    uniqueYears.sort((a, b) => a - b)
  
    //The amount of rows that are on the screen and adjust the height of the viewbox to it
    const numRows = uniqueYears.length / 10
    const requiredHeight = numRows * 130 + 210
    d3.select('#circles_svg').attr('height', '100%')
      .attr('viewBox', `0 0 ${svgWidthMap} ${requiredHeight}`)
  
    //Create a map to store occurrences for each year
    var occurrencesMap = d3.rollup(allCrashes, entries => entries.length, d => d.year)
    const radiusScale = d3.scaleLinear()
      .domain([d3.min(uniqueYears, year => occurrencesMap.get(year)), d3.max(uniqueYears, year => occurrencesMap.get(year))])
      .range([15, 50])
  
    //Display circles for each year on the circle map
    const yearCircles = circleMap.selectAll('.year-circle')
    .data(uniqueYears)
    .enter()
    .append('circle')
    .attr('class', 'year-circle')
    .attr('cx', (d, i) => (i % 10) * 120 + 50)
    .attr('cy', (d, i) => Math.floor(i / 10) * 130 + 150)
    .attr('r', d => radiusScale(occurrencesMap.get(d)))
    .style('fill', '#c94f4f')
    .style('cursor', 'pointer')
    .on('mouseover', mouseover)
    .on('mousemove', mousemove) 
    .on('mouseleave', mouseleave)
    .on('click', function (event, d) {
      var selectedYear = d
      updateCharts(selectedYear, selectedCountry)
      showYearInformation(selectedYear)})
  
    //Display year labels next to the circles
    const yearLabels = circleMap.selectAll('.year-label')
      .data(uniqueYears)
      .enter()
      .append('text')
      .attr('class', 'year-label')
      .attr('x', (d, i) => (i % 10) * 120 + 50)
      .attr('y', (d, i) => Math.floor(i / 10) * 130 + 154)
      .attr('text-anchor', 'middle')
      .style('cursor', 'pointer')
      .style('font-size', '14px')
      .text(d => d)
      .on('mouseover', mouseover)
      .on('mousemove', mousemove) 
      .on('mouseleave', mouseleave)
      .on('click', function (event, d) {
        var selectedYear = d
        updateCharts(selectedYear, selectedCountry)
        showYearInformation(selectedYear)})
  
    //The go back button on the circle map
    const circleButton = circleMap.append('rect')
      .attr('x', svgWidthMap / 2 - 100)
      .attr('y', 2)
      .attr('width', 200)
      .attr('height', 50)
      .attr('stroke-radius', 25)
      .style('fill', 'white')
      .style('cursor', 'pointer')
      .style('stroke', 'black')
      .style('rx', '30px')
      .on('click', goBack)
  
    //The year inside of the circle buttons
    const circleText = circleMap.append('text')
      .attr('x', svgWidthMap / 2)
      .attr('y', 37)
      .style('font-size', '28px')
      .style('font-weight', 'bold')
      .style('fill', 'black')
      .attr('text-anchor', 'middle')
      .style('cursor', 'pointer')
      .text('Go back')
      .on('click', goBack)

    //Hovering over the circles
    var tooltip = d3.select('#circle-chart')
      .append('div')
      .attr('class', 'tooltip')
      .style('background-color', 'white')
      .style('border', 'solid')
      .style('border-width', '1px')
      .style('border-radius', '10px')
      .style('padding', '10px')

    //Information that is shown whenever you hover over a circle
    function mouseover(event, d) {
      var selectedYear = d
      var yearCrashes = allCrashes.filter(entry => entry.year === selectedYear)
  
      var totalSurvivors = d3.sum(yearCrashes, d => d.Survived)
      var totalFatalities = d3.sum(yearCrashes, d => d.Fatalities)
      var totalCrashes = yearCrashes.length
  
      tooltip.html('Year: ' + selectedYear + '<br>' +
        'Total Crashes: ' + totalCrashes + '<br>' +
        'Total Fatalities: ' + totalFatalities + '<br>' +
        'Total Survivors: ' + totalSurvivors)
        .style('display', 'block')
        .style('opacity', 1)}
  
    //Keeping track of where the mouse moves and adjust the tooltip to it
    function mousemove(event, d) {
      var x = event.pageX
      var y = event.pageY
      tooltip.style('left', (x + 10) + 'px')
        .style('top', (y - 30) + 'px')}
  
    //Hiding the tooltip whenever you leave the circles
    function mouseleave() {
      tooltip.style('display', 'none')}

    //Hiding the map and showing a table with all its crashes on the bottom of the page
    function showYearInformation(selectedYear){
      circleMap.style('display', 'none')
      document.getElementById('bottom-row').style.display = 'none'
      document.getElementById('bottom-row-2').style.display = 'block'

      //Fetch the selected country value
      var selectedCountryText = document.getElementById('country-name').innerHTML
      var selectedCountry = selectedCountryText.split(': ')[1]
  
      //Create a button on the table page
      var backButton = document.createElement('button')
      backButton.id = 'goBack'
      backButton.innerText = 'Go back'
      backButton.addEventListener('click', goBack)
      document.getElementById('flight-info').appendChild(backButton)
      flightButton = document.getElementById('goBack').style.display= 'block'

      var flightInformation = data.filter(entry => entry.year === selectedYear)

      //Sort by month
      flightInformation.sort((a, b) => {
        var monthsOrder = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
        return monthsOrder.indexOf(a.Month)- monthsOrder.indexOf(b.Month)})

      //Showing the table
      yearFlightInfo.style('display', 'block')
      var info = document.getElementById('flight-Info-Div')

      var table = document.createElement('table')
      var headers = table.insertRow()
      var colNames = ['Operator', 'Country', 'Aboard','Survived', 'Fatalities', 'Month', 'Day', 'Summary']

      //Adding all values based on the name of the column
      colNames.forEach(columnName => {
        var header = headers.insertCell()
        header.textContent = columnName
        header.classList.add('header-cells')
        if (columnName === 'Summary' || columnName === 'Type' || columnName === 'Day' || columnName ==='Month' || columnName === 'Aboard') {
          header.classList.add('summary-column')}})
      
      //Start with 5 rows so that it does not get crowded
      var rowCount = 5
      flightInformation.slice(0,rowCount).forEach(flight=> {
        var rows = table.insertRow()
        colNames.forEach(columnName=>{
          var cell = rows.insertCell()
          cell.textContent = flight[columnName]
          cell.classList.add('row-cells')
          if (columnName === 'Summary' || columnName === 'Type' || columnName === 'Day' || columnName ==='Month' || columnName === 'Aboard') {
            cell.classList.add('summary-column')}})
      countryValue(selectedCountry, selectedYear)
      })
      info.innerHTML = ''

      var buttonWrap = document.createElement('div')
      buttonWrap.style.textAlign = 'center'
    
      //Add a button to show more than the 5 rows
      if (flightInformation.length > rowCount) {
        var showMoreButton = document.createElement('button')
        showMoreButton.id = 'showMore'
        showMoreButton.innerText = 'Show more'
        showMoreButton.addEventListener('click', function(){
          table.innerHTML = ''
          table.appendChild(headers)
          flightInformation.forEach((flight)=>{
              var rows = table.insertRow()
              colNames.forEach((columnName)=>{
                var cell = rows.insertCell()
                cell.textContent = flight[columnName]
                cell.classList.add('row-cells')
                if (columnName === 'Summary' || columnName === 'Type' || columnName === 'Day' || columnName ==='Month' || columnName === 'Aboard') {
                  cell.classList.add('summary-column')}
          })
        })
        showMoreButton.style.display = 'none'
      })
      buttonWrap.appendChild(showMoreButton)
    }
    info.appendChild(table)
    info.appendChild(buttonWrap)
  }
}

//Create the button to show world-wide information
var buttonDiv = document.getElementById('buttonall-div')
var buttonAll = document.createElement('button')
buttonAll.innerHTML = 'Show world information'
buttonAll.onclick = allCountries
buttonAll.id = 'showAllCountries'
buttonDiv.appendChild(buttonAll)

//////////////////////////////////////////////////////////////////
// Line-graph ///////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////

//Sizes of SVG and the margins
const margin = { top: 50, bottom: 50, right: 30, left: 50 }
const svgWidth = 800 - margin.left - margin.right
const svgHeight = 600 - margin.top - margin.bottom

//Create SVG container
svgLine = d3.select('#line-graph')
  .append('svg')
  .attr('width','100%')
  .attr('height','100%')
  //The line below is used so that the SVG is still covering the container on smaller screens
  .attr('viewBox',`0 0 ${svgWidth + margin.left + margin.right} ${svgHeight + margin.top + margin.bottom}`)
  .append('g')
  .attr('transform','translate(' + margin.left + ',' + margin.top + ')')

//Function to create the linegraph based on year and country
function createLineGraph(selectedYear,selectedCountry){
  //Clear the previously created line graph
  svgLine.selectAll('*').remove()

  var dataFiltered

  //If a country is selected, adjust the filter to filter the data for that country
  if (selectedCountry === 'All countries') {
    dataFiltered = data} 
  else {
    dataFiltered = data.filter(d => d.Country === selectedCountry)}

  // Creating an array containing the years and amount of crashes
  var countByYear = function (data, firstYear, lastYear) {
    var counts = []

    for (var year = firstYear; year <= lastYear; year++) {
      var count = data.filter(array => array.year === year).length
      counts.push({ year: year, count: count })}
    return counts}

  var counts = countByYear(dataFiltered, 1945, 2008)

  //Create scales and axes 
  let xScaleLine = d3.scaleLinear()
    .domain([1945, 2010])
    .range([0, svgWidth])
  
  let yScaleLine = d3.scaleLinear()
    .domain([0, d3.max(counts, d => d.count)])
    .range([svgHeight, 0])
  
  let xAxisLine = d3.axisBottom(xScaleLine).tickFormat(d3.format(''))
  let yAxisLine = d3.axisLeft(yScaleLine).tickFormat(function(d) {
    if (Number.isInteger(d)) {
      return d3.format('d')(d)} 
    else {
      return ''}})
  
  //Append axes
  svgLine.append('g').attr('transform','translate(0, ' + (svgHeight) + ')').call(xAxisLine)
  svgLine.append('g').call(yAxisLine)

  //Add tooltip whenever hovering over the line
  svgLine.append('rect')
    .style('fill', 'none')
    .style('pointer-events', 'all')
    .attr('width', svgWidth)
    .attr('height', svgHeight)
    .on('mouseover', mouseover)
    .on('mousemove', mousemove)
    .on('mouseout', mouseout)

  //Create line
  let lineLine = d3.line()
    .x(d => xScaleLine(d.year))
    .y(d => yScaleLine(d.count))

  //Append line to the SVG
  svgLine.append('path')
    .data([counts])
    .attr('fill', 'none')
    .attr('stroke', '#cb181d')
    .attr('stroke-width', 1)
    .attr('d', lineLine)

  //Add an extra circle add the selected year to put an emphasize on it
  if (selectedYear !== '--ALL YEARS--') {
    var selectedYearData = counts.find(d => d.year === selectedYear)

    svgLine.append('path')
      .data([[selectedYearData]])
      .attr('fill', 'none')
      .attr('stroke', 'blue')
      .attr('stroke-width', 2)
      .attr('d', lineLine)

    svgLine.append('circle')
      .data([selectedYearData])
      .attr('cx', d => xScaleLine(d.year))
      .attr('cy', d => yScaleLine(d.count))
      .attr('r', 7)
      .attr('fill', 'blue')}

  //Figure caption
  svgLine.append('text')
  .attr('x',svgWidth / 2)
  .attr('y',-margin.top / 2 - 2)
  .attr('text-anchor','middle')
  .style('font-size','30px')
  .style('font-weight','bold')
  .style('fill','black')
  .text('Aviation crashes per year')

  svgLine.append('text')
    .text('From 1945 until 2008')
    .attr('x', 350)
    .attr('y', -4)
    .style('font-size', '20px')
    .style('fill', 'black')
    .attr('text-anchor', 'middle')

  //x-Axis label
  svgLine.append('text')
  .attr('x',svgWidth / 2)
  .attr('y',svgHeight + margin.bottom - 10)
  .attr('text-anchor','middle')
  .style('font-size','20px')
  .text('Year')

  //y-Axis label
  svgLine.append('text')
  .attr('transform','rotate(-90)') 
  .attr('x',-svgHeight / 2) 
  .attr('y',-margin.left + 20) 
  .attr('text-anchor','middle')
  .style('font-size','20px')
  .text('Crashes')

  //Circle for the curves of the line graph
  var circleLine = svgLine.append('g')
    .append('circle')
    .style('fill', 'none')
    .attr('stroke', 'black')
    .attr('r', 11)
    .style('display', 'none')

  //Create the text for the circle
  var circleText = svgLine.append('g')
    .append('text')
    .style('display', 'none')

  //Showing the information (year, crashes) whenever hovering over the line
  function mouseover() {
    circleLine.style('display', 'block')
    circleText.style('display', 'block')}

  //Letting the information follow the mouse pointer
  function mousemove(event) {
    var x = xScaleLine.invert(d3.pointer(event)[0])

  //First data point
  var closestData = counts[0]

  //Find the closest data based on x (by calculating absolute values between data point and the selected x)
  for (var i = 1; i < counts.length; i++) {
    if (Math.abs(counts[i].year - x) < Math.abs(closestData.year - x)) {
      closestData = counts[i]}}
    
    circleLine.attr('cx', xScaleLine(closestData.year))
      .attr('cy', yScaleLine(closestData.count))    
    circleText.html(`Year: ${closestData.year} - Crashes: ${closestData.count}`)
  
  //Position the text dynamically
  var textWidth = circleText.node().getComputedTextLength()
  var textX = xScaleLine(closestData.year) + 20
  var textY = yScaleLine(closestData.count) + 7

  //Adjust the text position if it goes beyond the right edge of the SVG
  if (textX + textWidth > svgWidth) {
    textX = svgWidth - textWidth - 10}

  // Set the text position
  circleText.attr('x', textX).attr('y', textY)}

  //Hiding the information whenever not hovering over the graph
  function mouseout() {
    circleLine.style('display', 'none')
    circleText.style('display', 'none')}
}

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
//End of Line-Graph
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
//Start Pie-Chart
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

//Count the crashes per month
var countByMonth = function (data) {
    var counts = []
    for (var i = 0; i < months.length; i++) {
        var count = data.filter(array => array.Month === months[i]).length
        counts.push({ month: months[i], count: count })
    }
    return counts
  }

//Set pie-chart sizes
var width = 800
var height = 600
var marginPie = 70
var radius = Math.min(width, height)/ 2 -marginPie

//Add SVG area to #pie-chart div
var svgPieChart = d3.select('#pie-chart')
    .append('svg')
    .attr('id', 'pie_svg')
    .attr('width', '100%')
    .attr('height', '100%')
    .attr('viewBox', `0 0 ${svgWidth} ${svgHeight+margin.top+margin.bottom}`)
    .append('g')
    .attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')')

//Calculating crash percentages
function crashPercentage(count, totalCrashes) {
    return ((count / totalCrashes) * 100).toFixed(1) + '%'}

//Function to create the piechart using the results and totalcrashes
function drawPieChart(result, totalCrashes) {

  //Clear the pie chart
  svgPieChart.selectAll('*').remove()
    //Add a text caption above the pie chart
    svgPieChart.append('text')
      .attr('x', 0)
      .attr('y', -245)
      .attr('text-anchor', 'middle')
      .style('font-size', '25px')
      .style('font-weight', 'bold')
      .style('fill', 'black')
      .text('Aviation crashes by month')
    
    //Create a tooltip for hovering over the piechart slices
    var tooltip = d3.select('#pie-chart')
      .append('div')
      .attr('class', 'tooltip')
      .style('background-color', 'white')
      .style('border', 'solid')
      .style('border-width', '1px')
      .style('border-radius', '10px')
      .style('padding', '10px')    

    //Draw the pie chart if the crash amount is greater than 0
    if (totalCrashes > 0) {
        //The colorscale, the higher a value is the more red a slice is
        var colors = d3.scaleLinear()
          .domain([d3.min(result,d=>d.count), d3.max(result,d=>d.count)])
          .range([ '#E1B3B7','#99000d'])

        var pieValue = d3.pie().value(function (d) { return d.count}).sort(null)

        //Array containing the month and count
        var pieArray = pieValue(result)

        //Used for drawing the lines of the slices
        var pieLines = d3.arc()
            .innerRadius(150)
            .outerRadius(radius)
        
        //Mouse events
        function mouseOver(event, d) {
          var percentage = crashPercentage(d.data.count, totalCrashes)
          tooltip.transition()
            .duration(200)
            .style('opacity', 0.9)
            .style('left', (event.pageX) + 'px')
            .style('top', (event.pageY - 28) + 'px')
            .style('display', 'block')
            .text(d.data.month + ': '+ percentage)}

        function mouseMove(event, d) {
          var x = event.pageX
          var y = event.pageY
          tooltip.style('left', (x + 10) + 'px')
              .style('top', (y - 30) + 'px')}
      
        function mouseOut() {
          tooltip.transition().style('display', 'none')}
        
        //Creating the slices and their mouse event effects
        svgPieChart.selectAll('slices')
            .data(pieArray)
            .enter()
            .append('path')
            .filter(d => d.data.count > 0)
            .attr('d', pieLines)
            .attr('fill', function (d) { return colors(d.data.count)})
            .attr('stroke', 'white')
            .style('stroke-width', '3px')
            .style('opacity', 1)
            .on('mouseover', mouseOver)
            .on('mousemove', mouseMove)
            .on('mouseout', mouseOut)

        //Add labels with month names
        svgPieChart.selectAll('labels')
            .data(pieArray)
            .enter()
            .filter(d => d.data.count > 0)
            .append('text')
            .text(function (d) { return d.data.month})
            .attr('transform', function (d) { return 'translate(' + pieLines.centroid(d) + ')'})
            .style('text-anchor', 'middle')
            .style('font-size', 15)
            .style('font-weight', 'bold')
            .style('fill', 'black')
            .on('mouseover', mouseOver)
            .on('mousemove', mouseMove)
            .on('mouseout', mouseOut)
            
        //Add labels with counts below each month
        svgPieChart.selectAll('countLabels')
            .data(pieArray)
            .enter()
            .filter(d => d.data.count > 0)
            .append('text')
            .text(function (d) { return d.data.count})
            .attr('transform', function (d) {
                var position = pieLines.centroid(d)
                position[1] += 15
                return 'translate(' + position + ')'})
            .style('text-anchor', 'middle')
            .style('font-size', 13)
            .style('font-weight', 'bold')
            .style('fill', 'black')
            .on('mouseover', mouseOver)
            .on('mousemove', mouseMove)
            .on('mouseout', mouseOut)
    }
}
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
//End of donut-chart
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
//Start of Bar-Chart
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

//Set up horizontal bar chart dimensions and create SVG
var barChartWidth = 800
var barChartHeight = 600
var marginBarChart = {top: 90, right: 20, bottom: 40, left: 120}
var svgBarChart = d3.select('#barchart')
  .append('svg')
  .attr('id','bar_svg')
  .attr('width','100%')
  .attr('height','100%')
  .attr('viewBox',`0 0 ${barChartWidth} ${barChartHeight}`)
  .style('display','block')
  .append('g')
  .attr('transform','translate('+ marginBarChart.left + ',' + marginBarChart.top +')') 

//Function to create the horizontalbarchart using the selectedyear and selectedcountry
function createHorizontalBarChart(selectedYear,selectedCountry) {
//Clear the bar chart
  svgBarChart.selectAll('*').remove()

  //Filter data across all years and countries
  var filteredData
  if (isNaN(selectedYear) && selectedCountry === 'All countries') {
    filteredData = data
  } else if (!isNaN(selectedYear) && selectedCountry === 'All countries') {
    filteredData = data.filter(d => d.year === selectedYear)
  } else if (!isNaN(selectedYear) && selectedCountry !== 'All countries') {
    filteredData = data.filter(d => d.year === selectedYear && d.Country === selectedCountry)
  } else if (isNaN(selectedYear) && selectedCountry !== 'All countries') {
    filteredData = data.filter(d => d.Country === selectedCountry)}
  
  //Group data by operator and calculate the total fatalities
  var operatorData = d3.group(filteredData,d => d.Operator)
  var operatorsData = Array.from(operatorData,([operator, entries])=> 
  ({
    operator: operator,
    totalFatalities: d3.sum(entries, d => d.Fatalities),
    amountCrashed: entries.length}))

  //Sort operatorsData by totalFatalities
  operatorsData.sort((a, b) => b.totalFatalities - a.totalFatalities)
  var filteredOperatorsData = operatorsData.filter(d => d.totalFatalities > 10)
  //If there are less than 15 bars for fatalities, decrease the filter to 0
  //Adjust filters based on the amount of bars that would be displayed
  if (filteredOperatorsData.length < 15) {
    filteredOperatorsData = operatorsData.filter(d => d.totalFatalities >= 0)
    if (filteredOperatorsData.length > 15 ){ filteredOperatorsData = operatorsData.filter(d=> d.totalFatalities >= 5)}}
  else if (filteredOperatorsData.length > 25) {
    filteredOperatorsData = operatorsData.filter(d => d.totalFatalities > 75)
    if (filteredOperatorsData.length > 25) {filteredOperatorsData = operatorsData.filter(d=> d.totalFatalities > 150)}
      if (filteredOperatorsData.length > 20) {filteredOperatorsData = operatorsData.filter(d=> d.totalFatalities > 300)}
        if (filteredOperatorsData.length > 20) {filteredOperatorsData = operatorsData.filter(d=> d.totalFatalities > 500)}
    else if (filteredOperatorsData.length >= 15) {filteredOperatorsData = operatorsData.filter(d=> d.totalFatalities > 125)}
    if (filteredOperatorsData.length <= 5) {filteredOperatorsData = operatorsData.filter(d=> d.totalFatalities > 20)}}
  else if (filteredOperatorsData.length > 15){
    filteredOperatorsData = operatorsData.filter(d => d.totalFatalities > 50)
    if (filteredOperatorsData.length <=5) {filteredOperatorsData = operatorsData.filter(d => d.totalFatalities >25)}}

  //Scales for bar-chart
  var xScale = d3.scaleLinear()
    .domain([0,d3.max(filteredOperatorsData,d=> d.totalFatalities)])
    .range([0,barChartWidth - marginBarChart.left - marginBarChart.right])

    var operatorName = filteredOperatorsData.map(function(d, i) {
      if (d.operator.length > 10) {
          return d.operator.slice(0, 10) + '.._' + i} 
      else {
          return d.operator + '_' + i}})
  //Fetching the y values (scaleBand is used to make sure the labels of operators properly fit on the axis)
  var yScale = d3.scaleBand()
    .domain(operatorName)
    .range([0, barChartHeight - marginBarChart.top - marginBarChart.bottom])
    .padding(0.2)

  //Tooltip that shows when hovering over the bars
  var tooltip = d3.select('#barchart')
    .append('div')
    .attr('class', 'tooltip')
    .style('background-color', 'white')
    .style('border', 'solid')
    .style('border-width', '1px')
    .style('border-radius', '10px')
    .style('padding', '10px')

  //Three functions that change the tooltip
  var mouseover = function (event, d) {
    var totalCrashes = d.amountCrashed
    tooltip.html('Operator name: ' + d.operator + '<br>Amount of aviation crashes:  ' + totalCrashes + '<br>Amount of fatalities: ' + d.totalFatalities )
      .style('opacity', 1).style('display', 'block')}

  var mousemove = function (event, d) {
    var x = event.pageX
    var y = event.pageY
    tooltip.style('left', (x + 10) + 'px')
      .style('top', (y - 30) + 'px')}

  var mouseleave = function () {
    tooltip.style('display', 'none')}

  //Draw horizontal bars
  svgBarChart.selectAll('rect')
    .data(filteredOperatorsData)
    .enter()
    .append('rect')
    .attr('x',0)
    .attr('y', (d, i) => yScale(operatorName[i]))
    .attr('width',d => xScale(d.totalFatalities))
    .attr('height',yScale.bandwidth())
    .attr('fill','#cb181d')
    .on('mouseover', mouseover)
    .on('mousemove',mousemove)
    .on('mouseleave',mouseleave)

  //Add labels
  svgBarChart.selectAll('text')
    .data(filteredOperatorsData)
    .enter()
    .append('text')
    .text(d => d.totalFatalities)
    .attr('x',d=>xScale(d.totalFatalities/2))
    .attr('y', (d, i) => yScale(operatorName[i])+ yScale.bandwidth() / 2)
    .style('font-size','12px')
    .style('font-weight','bold')
    .style('alignment-baseline','middle')
    .style('text-anchor','middle')
    .style('fill','black')
    .on('mouseover', mouseover)
    .on('mousemove',mousemove)
    .on('mouseleave',mouseleave)
  
  //Add the title/captions above and next to the graph  
  svgBarChart.append('text')
    .text('Aviation fatalities on board per operator')
    .attr('x',barChartWidth / 2 - 150)
    .attr('y',-marginBarChart.top / 2)
    .style('font-size','30px')
    .style('font-weight','bold')
    .style('fill','black')
    .attr('text-anchor','middle')

    svgBarChart.append('text')
    .text('Sorted on the amount of fatalities')
    .attr('x', barChartWidth / 2 - 150)
    .attr('y', -marginBarChart.top / 2 + 30) 
    .style('font-size', '20px') 
    .style('fill', 'black')
    .attr('text-anchor', 'middle')

  svgBarChart.append('text')
    .text('Operators')
    .attr('transform','rotate(-90)')
    .attr('x',-barChartHeight / 2 + 100)
    .attr('y', '-100px')
    .attr('text-anchor','middle')
    .style('font-size','20px')
    .style('fill','black')

  svgBarChart.append('text')
    .text('Fatalities')
    .attr('x',barChartWidth / 2 - 150)
    .attr('y',barChartHeight - marginBarChart.bottom / 2 - 75)
    .attr('text-anchor','middle')
    .style('font-size','20px')
    .style('fill','black')
  
  //Add axes
  var xAxis = d3.axisBottom(xScale)
  var yAxis = d3.axisLeft(yScale)

  svgBarChart.append('g')
    .attr('transform','translate(0,'+(barChartHeight - marginBarChart.top - marginBarChart.bottom)+')')
    .call(xAxis)

  svgBarChart.append('g')
    .call(yAxis)
  }

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
//End of Bar-Chart
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////
//Update chart function
////////////////////////////////////////////////////////////////////////////
//Data gets filtered by looking at the selectedyear and country, recreates the visualizations using those values.
function updateCharts(selectedYear,selectedCountry){
  if (isNaN(selectedYear) && selectedCountry !== 'All countries'){
    var result = countByMonth(data.filter(data=>data.Country===selectedCountry))
    var totalCrashes = result.reduce((sum,data)=>sum+data.count,0)

    visualizationVisible =  d3.select('#column-left')
    if (visualizationVisible.style('display') === 'none'){
    d3.select('#column-left').style('display','block')
    d3.select('#column-left2').style('display','block')
    d3.select('#column-right').style('display','block')
    d3.select('#column-right2').style('display','block')}

    drawPieChart(result,totalCrashes)
    createLineGraph(selectedYear,selectedCountry)
    updateStackedBarChart(selectedYear, selectedCountry)
    createHorizontalBarChart(selectedYear, selectedCountry)}
  else if (!isNaN(selectedYear)&& selectedCountry === 'All countries'){
    var result = countByMonth(data.filter(data=>data.year === selectedYear))
    var totalCrashes = result.reduce((sum,data)=> sum + data.count,0)

    visualizationVisible =  d3.select('#column-left')
    if (visualizationVisible.style('display') === 'none'){
      d3.select('#column-left').style('display','block')
      d3.select('#column-left2').style('display','block')
      d3.select('#column-right').style('display','block')
      d3.select('#column-right2').style('display','block')}
  
    drawPieChart(result,totalCrashes)
    createLineGraph(selectedYear,selectedCountry)
    updateStackedBarChart(selectedYear, selectedCountry)
    createHorizontalBarChart(selectedYear, selectedCountry)}
  else if (!isNaN(selectedYear) && selectedCountry !== 'All countries'){
    var result = countByMonth(data.filter(data=>data.year===selectedYear&&data.Country===selectedCountry))
    var totalCrashes = result.reduce((sum,data)=>sum+data.count,0)

    visualizationVisible =  d3.select('#column-left')
    if (visualizationVisible.style('display') === 'none'){
    d3.select('#column-left').style('display','block')
    d3.select('#column-left2').style('display','block')
    d3.select('#column-right').style('display','block')
    d3.select('#column-right2').style('display','block')}

    drawPieChart(result,totalCrashes)
    createLineGraph(selectedYear,selectedCountry)
    updateStackedBarChart(selectedYear,selectedCountry)
    createHorizontalBarChart(selectedYear, selectedCountry)}
  else if (isNaN(selectedYear)&& selectedCountry === 'All countries'){
    var result = countByMonth(data)
    var totalCrashes = result.reduce((sum,data)=>sum+data.count,0)
    visualizationVisible =  d3.select('#column-left')
    if (visualizationVisible.style('display') === 'none'){
    d3.select('#column-left').style('display','block')
    d3.select('#column-left2').style('display','block')
    d3.select('#column-right').style('display','block')
    d3.select('#column-right2').style('display','block')}}

    drawPieChart(result,totalCrashes)
    createLineGraph(selectedYear,selectedCountry)
    updateStackedBarChart(selectedYear,selectedCountry)
    createHorizontalBarChart(selectedYear,selectedCountry)
  }
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
// End of bar-graph //
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
// Start of map //
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////

//SVG creation and text creation
var svgWidthMap = 1200
var svgHeightMap = 900
var centerX = svgWidthMap / 2
var centerY = svgHeightMap / 2

var svgMap = d3.select('#map-chart')
  .append('svg')
  .attr('id', 'map_svg')
  .attr('width', '100%')
  .attr('height', '100%')
  .attr('viewBox', `0 0 ${svgWidthMap} ${svgHeightMap}`)
  .append('g')

var circleMap = d3.select('#circle-chart')
  .append('svg')
  .attr('id', 'circles_svg')
  .attr('width', '100%')
  .attr('height', '100%')
  .attr('viewBox', `0 0 ${svgWidthMap} ${svgHeightMap}`)
  .style('display', 'none')

//Div creation for table
var yearFlightInfo = d3.select('#flight-info')
  .append('div')
  .attr('id', 'flight-Info-Div')
  .attr('width', '100%')
  .attr('height', '100%')
  .attr('viewBox', `0 0 ${svgWidthMap} ${svgHeightMap}`)
  .style('display', 'none')

//Function to show the information of a country when hovered on
var tooltip = d3.select('#map-chart')
.append('div')
.attr('class', 'tooltip')
.style('background-color', 'white')
.style('border', 'solid')
.style('border-width', '1px')
.style('border-radius', '10px')
.style('padding', '10px')

//Map
const projection = d3.geoMercator().scale(180).center([0, 20]).translate([centerX, centerY])

//Data and color scale
let dataMap = new Map()

//Colors and their scales
const colorScale = d3.scaleThreshold()
  .domain([0, 3, 15, 50, 150, 1000, 10000])
  .range(['white', ...d3.schemeReds[7].slice(1)])

//Load external data (from https://d3-graph-gallery.com/graph/choropleth_basic.html)
Promise.all([
  d3.json('https://raw.githubusercontent.com/holtzy/D3-graph-gallery/master/DATA/world.geojson'),
  d3.csv('../../data/countryOccurences.csv', function (d) {
    dataMap.set(d.Country_code, {
      //Set the values according to the selected country
      occurrences: +d.Occurences,
      fatalities: +d.Fatalities,
      survived: +d.Survived,
      country: d.Country})})
]).then(function (loadData) {
  let topo = loadData[0]

  //Three functions that change the tooltip
var mouseover = function (event, d) {
  countryData = dataMap.get(d.id)

  //All non selected countries will be more faded out
  d3.selectAll('.Country')
    .transition()
    .style('opacity', 0.5)
    .style('stroke', 'transparent')
  
  //Give the hovered over country a black border and a tooltip with information
  if(countryData){d.total = countryData.occurrences || 0
  d3.select(this).transition()
    .style('opacity', 1)
    .style('stroke', 'black')
    tooltip.html('Country: ' + countryData.country + '<br>' +
    'Amount of crashes: ' + countryData.occurrences + '<br>' +
      'Total Fatalities: ' + countryData.fatalities + '<br>' +
      'Total Survivors: ' + countryData.survived)
      .style('display', 'block').style('opacity', 1)}
  else {
  //Show text whenever a country has no data
  d.total = 0
  d3.select(this).transition()
    .style('opacity', 1)
    .style('stroke', 'black')
  tooltip.html('No data available for this country.').style('display','block').style('opacity',1)}
}

//Place the tooltip wherever the mouse moves to
var mousemove = function(event,d) {
  var x = event.pageX
  var y = event.pageY
  tooltip.style('left', (x +10) + 'px')
    .style('top', (y - 30) + 'px')}

//Hide the tooltip when leaving a country
var mouseleave = function() {
  d3.selectAll('.Country').style('opacity', 1)
  d3.select(this).style('stroke', 'transparent')
  tooltip.style('display', 'none')}

  //Puts the selected country from the map on top of the webpage for circles
  let countryClick = function(event, d) {
    countryData = dataMap.get(d.id)
    
    if (countryData) {
        circleMap.selectAll('*').remove()
        
          d.total = countryData.occurrences || 0
        d3.select('#map-chart').style('display', 'none')
        document.getElementById('showAllCountries').style.display = 'none'
        
        //Show circle map and country text
        circleMap.style('display', 'block')
        var selectedCountry = countryData.country
        document.getElementById('country-name').innerHTML = 'Selected country: ' + selectedCountry

        //Fetches all unique years that a country has a crash in
        var uniqueYears = [...new Set(data.filter(entry => entry.Country_code === d.id).map(entry =>entry.year))]
        uniqueYears.sort((a,b)=> a- b)

      //Amount of rows and height, needed for proper viewbox
      const numRows = uniqueYears.length / 10
      const requiredHeight = numRows * 130 + 210
        
      d3.select('#circles_svg').attr('height', '100%')
        .attr('viewBox', `0 0 ${svgWidthMap} ${requiredHeight}`)

    //Filter data for the selected country
    var countryCrashes = data.filter(entry => entry.Country_code === d.id)

    //Extract years from the filtered data for the selected country
    var yearOccurrences = d3.rollup(countryCrashes, 
      entries => entries.length,
      d => d.year)

    //Create a map to store occurrences for each year
    var occurrencesMap = new Map(yearOccurrences)

    //Colors for the map, dark red if a country has the most crashes and gets lighter the less flights crashed.
    const radiusScale = d3.scaleLinear()
    .domain([d3.min(uniqueYears, year => occurrencesMap.get(year)), d3.max(uniqueYears, year => occurrencesMap.get(year))])
    .range([15, 50])

    //Tooltip creation
    var tooltip = d3.select('#circle-chart')
    .append('div')
    .attr('class', 'tooltip')
    .style('background-color', 'white')
    .style('border', 'solid')
    .style('border-width', '1px')
    .style('border-radius', '10px')
    .style('padding', '10px')
    
    //Three functions that change the tooltip
    var mouseover = function (event, d) {
      var selectedYear = d
      var yearCrashes = countryCrashes.filter(entry => entry.year === selectedYear)
  
      var totalSurvivors = d3.sum(yearCrashes, d => d.Survived)
      var totalFatalities = d3.sum(yearCrashes, d => d.Fatalities)
      var totalCrashes = yearCrashes.length
  
      tooltip.html('Year: ' + selectedYear + '<br>' +
      'Total Crashes: ' + totalCrashes + '<br>' +
          'Total Fatalities: ' + totalFatalities + '<br>' +
          'Total Survivors: ' + totalSurvivors)
          .style('display', 'block').style('opacity', 1)}

    var mousemove = function(event,d) {
      var x = event.pageX
      var y = event.pageY
      tooltip.style('left', (x +10) + 'px')
        .style('top', (y - 30) + 'px')}
  
    var mouseleave = function() {
      tooltip.style('display', 'none')}

    //Display circles for each year on the circle map
    const yearCircles = circleMap.selectAll('.year-circle')
      .data(uniqueYears)
      .enter()
      .append('circle')
      .attr('class','year-circle')
      .attr('cx',(d, i) => (i % 10) * 120 + 50)
      .attr('cy',(d, i) => Math.floor(i / 10) * 130 + 150)
      .attr('r',d => radiusScale(occurrencesMap.get(d)))
      .style('fill','#c94f4f')
      .style('cursor','pointer')
      .on('mouseover', mouseover)
      .on('mousemove',mousemove)
      .on('mouseleave',mouseleave)
      .on('click',function (event,d){
        var selectedYear = d
        var selectedCountryText = document.getElementById('country-name').innerHTML
        var selectedCountry = selectedCountryText.split(': ')[1]
        updateCharts(selectedYear, selectedCountry)
        showYearInformation(selectedYear)})
     
    //Function to create the table and buttons on the table div
    function showYearInformation(selectedYear){
      circleMap.style('display', 'none')
      document.getElementById('bottom-row').style.display = 'none'
      document.getElementById('bottom-row-2').style.display = 'block'

      var selectedCountryText = document.getElementById('country-name').innerHTML
      var selectedCountry = selectedCountryText.split(': ')[1]
  
      //Create a button on the table page
      var backButton = document.createElement('button')
      backButton.id = 'goBack'
      backButton.innerText = 'Go back'
      backButton.addEventListener('click', goBack)
      document.getElementById('flight-info').appendChild(backButton)
      flightButton = document.getElementById('goBack').style.display= 'block'

      //Filter data on the selected year
      var flightInformation = countryCrashes.filter(entry => entry.year === selectedYear)

      //Sort by month
      flightInformation.sort((a, b) => {
        var monthsOrder = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
        return monthsOrder.indexOf(a.Month)- monthsOrder.indexOf(b.Month)})

      //Unhide the div
      yearFlightInfo.style('display', 'block')
      var info = document.getElementById('flight-Info-Div')

      var table = document.createElement('table')
      var headers = table.insertRow()
      var colNames = ['Operator', 'Type', 'Aboard','Survived', 'Fatalities', 'Month', 'Day', 'Summary']

      //Add the columns in the table
      colNames.forEach(columnName => {
        var header = headers.insertCell()
        header.textContent = columnName
        header.classList.add('header-cells')
        if (columnName === 'Summary' || columnName === 'Type' || columnName === 'Day' || columnName ==='Month') {
          header.classList.add('summary-column')}
      })
      
      //Start with 5 rows and add the row information in the right column
      var rowCount = 5
      flightInformation.slice(0,rowCount).forEach(flight=> {
        var rows = table.insertRow()
        colNames.forEach(columnName=>{
          var cell = rows.insertCell()
          cell.textContent = flight[columnName]
          cell.classList.add('row-cells')
          if (columnName === 'Summary' || columnName === 'Type' || columnName === 'Day' || columnName ==='Month') {
            cell.classList.add('summary-column')}})
      countryValue(selectedCountry, selectedYear)
      })
      info.innerHTML = ''

      //Button div
      var buttonWrap = document.createElement('div')
      buttonWrap.style.textAlign = 'center'
    
      //Create a show more button if there is still more data than 5 rows
      if (flightInformation.length > rowCount) {
        var showMoreButton = document.createElement('button')
        showMoreButton.id = 'showMore'
        showMoreButton.innerText = 'Show more'
        showMoreButton.addEventListener('click', function(){
          table.innerHTML = ''
          table.appendChild(headers)
          flightInformation.forEach(flight=>{
              var rows = table.insertRow()
              colNames.forEach(columnName=>{
                var cell = rows.insertCell()
                cell.textContent = flight[columnName]
                cell.classList.add('row-cells')
                if (columnName === 'Summary' || columnName === 'Type' || columnName === 'Day' || columnName ==='Month') {
                  cell.classList.add('summary-column')}
          })
        })
        showMoreButton.style.display = 'none'
      })
      buttonWrap.appendChild(showMoreButton)
    }
    //append table and button
    info.appendChild(table)
    info.appendChild(buttonWrap)
  }

    //Display year labels next to the circles
    const yearLabels = circleMap.selectAll('.year-label')
      .data(uniqueYears)
      .enter()
      .append('text')
      .attr('class','year-label')
      .attr('x',(d, i) =>(i % 10) * 120 + 50)
      .attr('y',(d, i) =>Math.floor(i / 10) * 130 + 154) 
      .attr('text-anchor','middle')
      .style('cursor','pointer')
      .style('font-size','14px')
      .text(d => d)
      .on('mouseover', mouseover)
      .on('mousemove',mousemove)
      .on('mouseleave',mouseleave)
      .on('click',function (event,d){
        var selectedYear = d
        var selectedCountryText = document.getElementById('country-name').innerHTML
        var selectedCountry = selectedCountryText.split(': ')[1]
        updateCharts(selectedYear, selectedCountry)
        showYearInformation(selectedYear)})

    //The go back button on the circle map
    const circleButton = circleMap.append('rect')
      .attr('x',svgWidthMap / 2 - 100)
      .attr('y',2)
      .attr('width',200)
      .attr('height',50)
      .attr('stroke-radius', 25)
      .style('fill','white')
      .style('cursor','pointer')
      .style('stroke','black')
      .style('rx','30px')
      .on('click',goBack)
    
    //The text inside of the goback button
    const circleText= circleMap.append('text')
      .attr('x',svgWidthMap / 2)
      .attr('y', 37)
      .style('font-size','28px')
      .style('font-weight','bold')
      .style('fill','black')
      .attr('text-anchor','middle')
      .style('cursor','pointer')
      .text('Go back')
      .on('click', goBack)
    
    //Fetch selected country name from the div at the top
    var selectedYear = '--ALL YEARS--'
    updateCharts(selectedYear, selectedCountry)
    }    
    else {
      d.total = 0
      svgMap.style('display','block')
      countryValue(undefined)}
    }

  //Draw the map
  svgMap
    .append('g')
    .selectAll('path')
    .data(topo.features)
    .join('path')
    //Draw each country
    .attr('d', d3.geoPath().projection(projection))
    //Sets the color of a country, sets it to white if the country id is not found
    .attr('fill', function (d) {
      let countryData = dataMap.get(d.id)
      if (countryData) {
        d.total = countryData.occurrences
        return colorScale(d.total)} 
      else {
        d.total = 0
        return 'white'}
      })
    .style('stroke', 'transparent')
    .attr('class','Country')
    .style('opacity', 1.0)
    .on('mouseover', mouseover)
    .on('mousemove', mousemove)
    .on('mouseleave', mouseleave)
    .on('click', countryClick)

  //Create a legend
  const legend = svgMap.append('g').attr('class', 'legend').attr('transform', 'translate(20, 20)')

  //Create legend rectangles
  legend.selectAll('rect')
    .data(colorScale.range())
    .enter()
    .append('rect')
    .attr('x', 0)
    .attr('y', (d, i) => i * 20 + 9)
    .attr('width', 18)
    .attr('height', 18)
    .style('fill', (d) => d)

  //Legend labels
  const customLegendLabels = ['Zero Occurrences', '1-2', '3-14', '15-49', '50-149', '150-999', '1000+']
  legend.selectAll('text')
    .data(customLegendLabels)
    .enter()
    .append('text')
    .attr('x', 30)
    .attr('y', (d, i) => i * 20 + 19)
    .attr('dy', '.35em')
    .style('text-anchor', 'start')
    .text((d) => d)

  //Legend text
  svgMap.append('text')
    .attr('x', 160)
    .attr('y', 25)
    .attr('text-anchor', 'middle')
    .style('font-size', '20px')
    .style('font-weight', 'bold')
    .text('Map with crashes per country')
  })


/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
// End of map and table
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
// STACKED BAR CHART
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
//Function to create the stacked bar chart
function updateStackedBarChart(selectedYear,selectedCountry) {

  //Fetch the data required for the stacked bar chart by comparing country names
  var countryDataStacked
  if (selectedCountry !== 'All countries') {
    countryDataStacked = data.filter(d => d.Country === selectedCountry)
  } else {
    //Use data for all countries
    countryDataStacked = data}

  //Group data by operator, creates an array containing the data per operator
  var operatorDataStacked = d3.group(countryDataStacked, d => d.Operator)
  var operatorsDataStacked = Array.from(operatorDataStacked, ([operator, values]) => ({
    operator: operator,
    totalFatalities: d3.sum(values, d => d.Fatalities),
    totalSurvivors: d3.sum(values, d => d.Survived),
    amountCrashed: values.length}))

  // calculate the percentages of fatalities and survivors
  operatorsDataStacked.forEach(d => {
    d.percentageFatalities = (d.totalFatalities / (d.totalFatalities + d.totalSurvivors)) * 100
    d.percentageSurvivors = (d.totalSurvivors / (d.totalFatalities + d.totalSurvivors)) * 100})

  operatorsDataStacked.sort((a, b) => b.totalSurvivors - a.totalSurvivors)
  //If no year is selected, filter only on year instead of also on country
  if(selectedYear !== '--ALL YEARS--'){
    var yearDataStacked = countryDataStacked.filter(d=> d.year === selectedYear)

    var yearOperatorDataStacked = d3.group(yearDataStacked, d=> d.Operator)

    var yearOperatorsDataStacked = Array.from(yearOperatorDataStacked, ([operator, values]) => ({
      operator: operator,
      totalFatalities: d3.sum(values, d => d.Fatalities),
      totalSurvivors: d3.sum(values, d => d.Survived),
      amountCrashed: values.length}))
      
  // calculate the percentages of fatalities and survivors
  yearOperatorsDataStacked.forEach(d=> {
    d.percentageFatalities = (d.totalFatalities / (d.totalFatalities + d.totalSurvivors)) * 100
    d.percentageSurvivors = (d.totalSurvivors / (d.totalFatalities + d.totalSurvivors)) * 100})
  
  yearOperatorsDataStacked.sort((a,b) => b.totalSurvivors - a.totalSurvivors)

  var topOperatorsData = yearOperatorsDataStacked.slice(0,15)}
  else {
    var topOperatorsData = operatorsDataStacked.slice(0,15)}

  //Clear the stacked-bar div
  d3.select('#stacked-bar').select('svg').remove()

  //Sizes and margins of the graph
  var margin = { top: 50, bottom: 70, right: 30, left: 50 }
  var width = 800 - margin.left - margin.right
  var height = 500 - margin.top - margin.bottom

  //Append the svg object to the place of the stacked-bar
  var svgStackedBar = d3.select('#stacked-bar')
    .append('svg')
    .attr('width', '100%')
    .attr('height', height + margin.top + margin.bottom)
    .attr('viewBox', `0 0 ${svgWidth + margin.left + margin.right} ${svgHeight + margin.top + margin.bottom}`)
    .append('g')
    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')

  //Add X axis
  var x = d3.scaleBand()
  .domain(topOperatorsData.map(function(d,i) {
    if (d.operator.length > 10) {
      return d.operator.slice(0, 10) + '.._' + i} 
    else {
      return d.operator + '_' + i}
    }))
    .range([0, width])
    .padding([0.4])

  //Stacked bar chart elements, the bars and text
  svgStackedBar.append('g')
    .attr('transform', 'translate(0,' + height + ')')
    .call(d3.axisBottom(x).tickSizeOuter(0))
    .selectAll('text')
    .style('fill', 'black')
    .attr('transform', 'rotate(-75) translate(-6, -8)')
    .attr('text-anchor', 'end')
    .style('font-size', '14px')

  svgStackedBar.append('text')
    .text('Fatalities vs survivors per operator')
    .attr('x', 400)
    .attr('y', -28)
    .style('font-size', '30px')
    .style('font-weight', 'bold')
    .style('fill', 'black')
    .attr('text-anchor', 'middle')

    svgStackedBar.append('text')
      .text('Sorted on the amount of survivors')
      .attr('x', 400)
      .attr('y', -4)
      .style('font-size', '20px')
      .style('fill', 'black')
      .attr('text-anchor', 'middle')

    svgStackedBar.append('text')
      .attr('x',svgWidth / 2)
      .attr('y',svgHeight+50)
      .attr('text-anchor','middle')
      .style('font-size','20px')
      .text('Operators')

    svgStackedBar.append('text')
      .attr('transform','rotate(-90)') 
      .attr('x',-svgHeight / 2 + 60) 
      .attr('y',-margin.left + 20) 
      .attr('text-anchor','middle')
      .style('font-size','20px')
      .text('Percentages')

  //Add Y axis
  var y = d3.scaleLinear()
    .domain([0, 100])
    .range([height, 0])

  svgStackedBar.append('g')
    .call(d3.axisLeft(y))

  var color = d3.scaleOrdinal()
    .domain(['Fatalities', 'Survivors'])
    .range(['#cb181d', '#BCD7AE'])

  //Stack the data
  var stackedData = d3.stack()
    .keys(['percentageFatalities', 'percentageSurvivors'])
    (topOperatorsData)

  var tooltip = d3.select('#stacked-bar')
    .append('div')
    .attr('class', 'tooltip')
    .style('background-color', 'white')
    .style('border', 'solid')
    .style('border-width', '1px')
    .style('border-radius', '10px')
    .style('padding', '10px')

  //Three functions that change the tooltip
  var mouseover = function (event, d) {
    var subgroupName
    if (d3.select(this.parentNode).datum().key === 'percentageSurvivors')
      subgroupName = 'Survivor percentage'
    else {
      subgroupName = 'Fatality percentage'}
    var subgroupValue = d[1] - d[0]
    var totalSurvivors = d.data.totalSurvivors
    var totalFatalities = d.data.totalFatalities
    var totalCrashes = d.data.amountCrashed
    var roundedSubgroupValue = subgroupValue.toFixed(1)
    var operator = d.data.operator
    tooltip.html('Group: ' + subgroupName + '<br>' + 'Value: ' + roundedSubgroupValue + '%<br>' + 'Total survivors: '
      + totalSurvivors + '<br>' + 'Total fatalities: ' + totalFatalities + '<br>' + 'Amount of aviation crashes:  ' + totalCrashes +'<br>' + 'Operator: '+ operator)
      .style('opacity', 1).style('display', 'block')}

  var mousemove = function (event, d) {
    var x = event.pageX
    var y = event.pageY
    tooltip.style('left', (x + 10) + 'px')
      .style('top', (y - 30) + 'px')}

  var mouseleave = function () {
    tooltip.style('display', 'none')}

  //Show the bars and their info when hovering
  svgStackedBar.append('g')
    .selectAll('g')
    .data(stackedData)
    .enter().append('g')
    .attr('fill', function(d) { return color(d.key)})
    .selectAll('rect')
    .data(function(d) { return d})
    .enter().append('rect')
    .attr('x', function(d,i) {
      if (d.data.operator.length > 10) {
        operatorName = d.data.operator.slice(0, 10) + '.._' + i} 
      else {
        operatorName = d.data.operator+ '_' + i}
      return x(operatorName) + (x.bandwidth() / - x.bandwidth() / 2) /2})
    .attr('y', function(d) { return y(d[1])})
    .attr('height', function(d) { return y(d[0]) - y(d[1])})
    .attr('width', x.bandwidth())
    .on('mouseover', mouseover)
    .on('mousemove', mousemove)
    .on('mouseleave', mouseleave)}    

///////////////////////////////////////////////////////////////////////
// END OF STACKED BAR CHART ////////
///////////////////////////////////////////////////////////////////////

//Set country name at the top of the page depending on the clicked on country
function countryValue(selectedCountry, selectedYear){
  svgMap = d3.select('#map-chart')
  circleMap = d3.select('#circles_svg')
  if (svgMap.style('display') === 'block'){
    selectedCountry = 'All countries'
    document.getElementById('country-name').innerHTML = 'Selected country: ' + selectedCountry}
  else if (circleMap.style('display') === 'block'){
    selectedCountryNotSplit = selectedCountry
    document.getElementById('country-name').innerHTML = 'Selected country: ' + selectedCountry}
  else if (yearFlightInfo.style('display') === 'block'){
    selectedCountry = selectedCountry
    selectedYear = selectedYear
    document.getElementById('country-name').innerHTML = 'Selected country: ' + selectedCountry + '<br />' + 'Selected year: ' + selectedYear}}

//Functionality for the go back button, uses the display values to determine what to do next
function goBack() {
  //Selecting relevant elements using D3
  var yearFlightInfo = d3.select('#flight-Info-Div')
  var circleMap = d3.select('#circles_svg')
  var yearFlightButton = d3.select('#goBack')
  //Extracting the selected country from the displayed text
  var selectedCountryText = document.getElementById('country-name').innerHTML
  selectedCountry = selectedCountryText.split('Selected country: ')
  if (selectedCountry.length > 1) {
    selectedCountry = selectedCountry[1].split('<br>')[0]} 
    else {
    selectedCountry = selectedCountry}
  //Changing based on display mode
  if (yearFlightInfo.style('display') === 'block') {
    //If yearFlightInfo is displayed
    yearFlightInfo.style('display', 'none')
    yearFlightButton.style('display', 'none')
    document.getElementById('bottom-row-2').style.display = 'none'
    document.getElementById('bottom-row').style.display = 'block'
    circleMap.style('display', 'block')
    var selectedYear = '--ALL YEARS--'
    countryValue(selectedCountry)
    updateCharts(selectedYear, selectedCountry)
    //Adjusting the visibility of visualizations columns
    visualizationVisible = d3.select('#column-left')
    if (visualizationVisible.style('display') === 'none') {
      d3.select('#column-left').style('display', 'block')
      d3.select('#column-left2').style('display', 'block')
      d3.select('#column-right').style('display', 'block')
      d3.select('#column-right2').style('display', 'block')
    }
  } else if (circleMap.style('display') === 'block') {
    //If circleMap is displayed
    circleMap.style('display', 'none')
    d3.select('#map-chart').style('display', 'block')
    countryValue('All countries')
    selectedCountry = 'All countries'
    selectedYear = '--ALL YEARS--'
    updateCharts(selectedYear, selectedCountry)
    d3.select('#buttonall-div').style('display', 'initial')
    document.getElementById('country-name').innerHTML = 'Select a country:'
    //Adjusting the visibility of visualizations columns
    visualizationVisible = d3.select('#column-left')
    if (visualizationVisible.style('display') === 'none') {
      d3.select('#column-left').style('display', 'block')
      d3.select('#column-left2').style('display', 'block')
      d3.select('#column-right').style('display', 'block')
      d3.select('#column-right2').style('display', 'block')
    } else {
      d3.select('#column-left').style('display', 'none')
      d3.select('#column-left2').style('display', 'none')
      d3.select('#column-right').style('display', 'none')
      d3.select('#column-right2').style('display', 'none')
    }
    d3.select('#showAllCountries').style('display', 'initial')
  }
}
})
  