# INFOVIS

## Name
InfoVIS Crash Investigators Dashboard

## Description
This Git contains all files required to recreate our created dashboard. The documentation such as the [data understanding](/documentation/data understanding), [evaluation](/documentation/evaluation),  [infovis design](/documentation/infovis design) and [sketches](/documentation/sketches) phase of the project can be found in the [documentation](/documentation) folder. This documentation folder contains no code at all.

In the folder [data](/data) the datasets can be found, the 'Airplane_Crashes_and_Fatalities_Since_1908.csv' contains the original dataset which can also be found at 'https://www.kaggle.com/datasets/saurograndi/airplane-crashes-since-1908/'. The 'cleanedData.csv' contains all data used in our project, with all columns that have been used. The 'countryOccurences.csv' file contains the country names and country_codes with the amount of total crashes,fatalities,survivors in a country. This folder also contains no code.

The folder [code](/code) contains all code required to run the project. Inside of the [code](/code) folder, a [data_wrangling_scripts](/code/data wrangling scripts) folder can be found which contains the Python code that cleans the dataset to our likings. It requires two dependencies to be ran, namely pandas and pycountry. These dependencies can be downloaded through the means underneath the Installation header. The older folder inside of the [code](/code) folder, is [webapp](/code/webapp) which contains the HTML, CSS and JS. 

## Installation
Please use Visual Studio Code for the best results.
Before running the code you will need to install two dependencies. These can be installed by running the `pip install -r requirements.txt` command in the terminal.
Installing the extension Live Preview in Visual studio allows for a localhost to be ran and visualizing the website (use the preview tool at the top right in visual studio when the HTML file is selected).
The results can also be found [here](https://webspace.science.uu.nl/~1080288/infovis/code/webapp/) in case you are using a different environment and would rather not use an extension.


## Authors and acknowledgment
Group 4 - <br>
Matthijs Kamps <br>
Eric Maas <br>
Jeffrey Post <br>
Marc Rademakers <br>
Shanmukha Shinagam<br>

[Kaggle (Sauro Grandi)](https://www.kaggle.com/datasets/saurograndi/airplane-crashes-since-1908/) <br>
[D3 Graphs](https://d3-graph-gallery.com/)
